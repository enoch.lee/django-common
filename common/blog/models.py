# coding=utf-8
from __future__ import unicode_literals

import datetime
from django.db import models
from zinnia.models_bases.entry import AbstractEntry

from common.blog.settings import CATEGORY_DETAIL_TEMPLATES
from common.utils import FilenameChanger


class CategoryInformation(models.Model):
    DRAFT = 0
    HIDDEN = 1
    PUBLISHED = 2

    GROUP_CHOICES = [
        (-1, '모든 이용자'),
        (0, '비활성 로그인 이용자'),
        (1, '로그인 이용자'),
        (2, '운영자'),
        (3, '관리자'),
    ]
    DEFAULT_ENTRY_STATUS = [
        (DRAFT, '초안'),
        (PUBLISHED, '활성'),
    ]

    category = models.OneToOneField("zinnia.Category", primary_key=True, related_name="info")
    image = models.ImageField("메인 이미지", upload_to=FilenameChanger('zinnia/category'), blank=True)
    view_permitted_group = models.IntegerField("보기 권한", choices=GROUP_CHOICES, default=1)
    edit_permitted_group = models.IntegerField("글쓰기 권한", choices=GROUP_CHOICES, default=1)
    entry_default_status = models.IntegerField("기본 글 상태", choices=DEFAULT_ENTRY_STATUS, default=PUBLISHED)
    allow_password = models.BooleanField("비밀 게시글 허용", default=False)
    allow_anonymous = models.BooleanField("익명 허용", default=False)
    content_template = models.CharField("게시판 템플릿", max_length=250, default='default',
                                        choices=[('default', '기본 템플릿')] + CATEGORY_DETAIL_TEMPLATES)

    class Meta:
        verbose_name = "카테고리 추가정보"
        verbose_name_plural = "카테고리 추가정보"

    @property
    def login_required(self):
        return self.view_permitted_group > 0

    @staticmethod
    def _can_do(user, group):
        if user.is_superuser:
            return True
        elif user.is_staff and group <= 2:
            return True
        elif user.is_authenticated() and user.is_active and group <= 1:
            return True
        elif user.is_authenticated() and not user.is_active and group <= 0:
            return True
        elif group == -1:
            return True
        else:
            return False

    def can_view(self, user):
        return CategoryInformation._can_do(user, self.view_permitted_group)

    def can_edit(self, user):
        return CategoryInformation._can_do(user, self.edit_permitted_group)

    def has_recent_entries(self):
        most_recent_entry = self.category.entries_published().order_by('-creation_date').first()
        return most_recent_entry.is_recent() if most_recent_entry else None


class Entry(AbstractEntry):
    visits = models.IntegerField("방문자 수", default=0)
    is_anonymous = models.BooleanField("익명으로 작성", default=False)

    class Meta(AbstractEntry.Meta):
        abstract = True

    def writers(self):
        return "익명" if self.is_anonymous else ", ".join([author.__unicode__() for author in self.authors.all()])

    def is_recent(self):
        now = datetime.datetime.now()
        return (now - self.creation_date).days <= 2

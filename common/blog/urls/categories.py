from django.conf.urls import url
from ..views import CategoryDetail

urlpatterns = [
    url(r'^(?P<path>[-\/\w]+)/$', CategoryDetail.as_view(), name='category_detail'),
]

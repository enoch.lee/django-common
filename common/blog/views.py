# coding=utf-8
from __future__ import unicode_literals

from django.contrib.auth.mixins import UserPassesTestMixin
from django.views.generic import CreateView, UpdateView, DeleteView
from django.views.generic.edit import FormMixin
from django_comments.models import Comment
from tagging.models import Tag, TaggedItem
from zinnia.models import Category, Entry
from zinnia.views.categories import CategoryDetail as CategoryDetailParent
from zinnia.views.entries import EntryDetail as EntryDetailParent

from .forms import EntryForm, EntryFilterForm
from .models import CategoryInformation


class EntryViewMixin(object):
    template_name = "blog/entry-create.html"
    form_class = EntryForm
    category = None

    def get_success_url(self):
        return self.object.get_absolute_url()

    def get_context_data(self, **kwargs):
        context = super(EntryViewMixin, self).get_context_data(**kwargs)
        context['category'] = self.category
        return context

    def get_form_kwargs(self):
        kwargs = super(EntryViewMixin, self).get_form_kwargs()
        kwargs['category'] = self.category
        kwargs['user'] = self.request.user
        return kwargs

    def get_category(self):
        return Category.objects.get(id=self.kwargs['category_id'])

    def dispatch(self, request, *args, **kwargs):
        self.category = self.get_category()
        return super(EntryViewMixin, self).dispatch(request, *args, **kwargs)


class EntryCreate(UserPassesTestMixin, EntryViewMixin, CreateView):

    def test_func(self):
        category_info, created = CategoryInformation.objects.get_or_create(category=self.get_category())
        return category_info.can_edit(self.request.user)


class EntryUpdate(UserPassesTestMixin, EntryViewMixin, UpdateView):
    model = Entry

    # TODO initial data: 태그

    def test_func(self):
        return self.get_object().authors.filter(id=self.request.user.id).exists()


class EntryDelete(UserPassesTestMixin, DeleteView):
    template_name = "blog/entry-delete.html"
    model = Entry

    def get_success_url(self):
        return Category.objects.get(id=self.kwargs['category_id']).get_absolute_url()

    def test_func(self):
        return self.get_object().authors.filter(id=self.request.user.id).exists()


class EntryDetail(UserPassesTestMixin, EntryDetailParent):

    def get_context_data(self, **kwargs):
        context = super(EntryDetail, self).get_context_data(**kwargs)
        # context['category'] =
        return context

    def test_func(self):
        return self.get_object().categories.order_by('info__view_permitted_group').first().info.can_view(self.request.user)


class EntryCommentDelete(UserPassesTestMixin, DeleteView):
    template_name = "blog/entry-comment-delete.html"
    model = Comment

    def get_success_url(self):
        return self.object.content_object.get_absolute_url()

    def test_func(self):
        return self.get_object().user == self.request.user


class CategoryDetail(FormMixin, CategoryDetailParent):
    form_class = EntryFilterForm

    def get_template_names(self):
        templates = super(CategoryDetail, self).get_template_names()
        if self.category.info.content_template != 'default':
            templates.insert(0, self.category.info.content_template)
        return templates

    def get_queryset(self):
        queryset = super(CategoryDetail, self).get_queryset()
        search = self.request.GET.get("search", "")
        tag_id = self.request.GET.get("tag")
        searched_queryset = queryset.filter(title__icontains=search)

        if tag_id:
            return TaggedItem.objects.get_by_model(searched_queryset, Tag.objects.get(id=tag_id))
        else:
            return searched_queryset

    def get_form_kwargs(self):
        kwargs = super(CategoryDetail, self).get_form_kwargs()
        kwargs['category'] = self.category
        return kwargs

    def get_initial(self):
        return {'tag': self.request.GET.get("tag"), 'search': self.request.GET.get("search")}

    def get_context_data(self, **kwargs):
        context = super(CategoryDetail, self).get_context_data(**kwargs)
        context['featured_entries'] = self.category.entries_published().filter(featured=True)
        return context

# coding=utf-8
import itertools

from django.db.models import F
from django.db.models.signals import pre_save, post_save, m2m_changed, pre_delete
from django.dispatch import receiver
from django_comments.models import Comment
from slugify import slugify
from zinnia.models import Entry, Category
from .models import CategoryInformation


@receiver(pre_save, sender=Entry)
@receiver(pre_save, sender=Category)
def set_slug(sender, instance, **kwargs):
    instance.slug = slug = slugify(instance.title)
    for x in itertools.count(1):
        if not sender.objects.filter(slug=instance.slug).exclude(id=instance.id).exists():
            break
        instance.slug = '%s-%d' % (slug, x)


@receiver(post_save, sender=Category)
def create_category_info(instance, created, **kwargs):
    if created or not CategoryInformation.objects.filter(category=instance).exists():
        CategoryInformation.objects.get_or_create(category=instance)


def set_login_required(instance, model, pk_set, **kwargs):
    categories = model.objects.filter(id__in=pk_set)
    instance.login_required = any(category.info.view_permitted_group > 0 for category in categories)

m2m_changed.connect(set_login_required, sender=Entry.categories.through)


@receiver(post_save, sender=CategoryInformation)
def category_info_changed(instance, created, **kwargs):
    instance.category.entries.all().update(login_required=instance.view_permitted_group > 0)


@receiver(pre_delete, sender=Comment)
def update_comments_count(sender, instance, using, **kwargs):
    if instance.is_public:
        entry = instance.content_object
        if isinstance(entry, Entry):
            entry.comment_count = F('comment_count') - 1
            entry.save(update_fields=['comment_count'])


from __future__ import unicode_literals
from django.apps import AppConfig


class BlogConfig(AppConfig):
    name = 'common.blog'

    def ready(self):
        import common.blog.signals
        import common.blog.settings
        import patch

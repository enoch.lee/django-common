from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline
from django_mptt_admin.admin import DjangoMpttAdmin
from tagging.models import TaggedItem
from zinnia.admin import CategoryAdmin as CategoryAdminParent, EntryAdmin as EntryAdminParent
from zinnia.models import Category, Entry

from .forms import EntryAdminForm
from .models import CategoryInformation
from django_comments.models import Comment
import django_comments.admin
admin.site.unregister(Comment)

admin.site.unregister(Category)


class CategoryTagInline(GenericTabularInline):
    model = TaggedItem
    extra = 0


class CategoryInformationInline(admin.StackedInline):
    model = CategoryInformation
    can_delete = False
    extra = 1


class CommentInline(GenericTabularInline):
    model = Comment
    extra = 0
    ct_fk_field = 'object_pk'
    fields = ['comment', 'user_name']
    readonly_fields = ['user_name']


@admin.register(Category)
class CategoryAdmin(DjangoMpttAdmin, CategoryAdminParent):
    fields = ('title', 'parent', 'description')
    list_display = ('title', 'get_tree_path', 'description')
    prepopulated_fields = {}
    inlines = [CategoryInformationInline, CategoryTagInline, ]


@admin.register(Entry)
class EntryAdmin(EntryAdminParent):
    fieldsets = (
        (_('Content'), {
            'fields': ('status', 'title', 'content', 'categories', 'tags', 'image', 'image_caption')}),
        (_('Publication'), {
            'fields': (('start_publication', 'end_publication'),
                       'creation_date', 'sites'),
            'classes': ('collapse', 'collapse-closed')}),
        (_('Discussions'), {
            'fields': ('comment_enabled',),
            'classes': ('collapse', 'collapse-closed')}),
        (_('Privacy'), {
            'fields': ('login_required', 'password'),
            'classes': ('collapse', 'collapse-closed')}),
        (_('Templates'), {
            'fields': ('content_template', 'detail_template'),
            'classes': ('collapse', 'collapse-closed')}),
        (_('Metadatas'), {
            'fields': ('featured', 'excerpt', 'authors', 'related'),
            'classes': ('collapse', 'collapse-closed')}))
    prepopulated_fields = {}
    form = EntryAdminForm
    inlines = [CommentInline]

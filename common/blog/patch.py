from __future__ import unicode_literals
from django_comments.models import Comment
from tagging.models import Tag
from zinnia.models import Category, Entry


def category_unicode(self):
    tags = ", ".join(list(Tag.objects.get_for_object(self).values_list("name", flat=True)))
    return "{} ({})".format(self.title, tags) if tags else self.title

Category.__unicode__ = category_unicode


def _get_name(self):
    return self.user_name or self.userinfo["name"]

Comment.writer = _get_name

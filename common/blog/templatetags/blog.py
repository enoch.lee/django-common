from django import template

register = template.Library()


@register.filter
def contextual_color(string):
    import random
    random.seed(string)
    number = random.randrange(1, 6)
    color = 'default'
    if number == 1:
        color = 'primary'
    elif number == 2:
        color = 'success'
    elif number == 3:
        color = 'info'
    elif number == 4:
        color = 'warning'
    elif number == 5:
        color = 'danger'
    return color

import os
import uuid

from django.utils.deconstruct import deconstructible


@deconstructible
class FilenameChanger(object):

    def __init__(self, base_path):
        self.base_path = base_path

    def __call__(self, instance, filename, *args, **kwargs):
        ext = filename.split('.')[-1].lower()
        filename = "%s.%s" % (uuid.uuid4(), ext)

        return os.path.join(self.base_path, filename)

    def __eq__(self, other):
        return self.base_path
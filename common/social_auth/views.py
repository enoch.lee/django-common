from django.conf import settings
from django.contrib.auth import authenticate as auth_authenticate, login as auth_login
from django.template.context_processors import csrf
from django.views.generic import RedirectView


class OAuth2Authentication(RedirectView):
    url = settings.LOGIN_REDIRECT_URL

    def get(self, request, *args, **kwargs):
        csrf_token = request.session.get('social_status')

        if csrf_token == request.GET.get('state'):
            code = request.GET.get("code")
            source = self.kwargs['source']
            user = auth_authenticate(code=code, source=source)

            if user:
                if user.is_active:
                    auth_login(request, user)
                else:
                    error = 'AUTH_DISABLED'
            else:
                error = 'AUTH_FAILED'

        self.url = request.GET.get('next', self.url)
        return super(OAuth2Authentication, self).get(request, *args, **kwargs)

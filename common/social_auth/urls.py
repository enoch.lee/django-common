# -*- coding: utf-8 -*-
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^oauth/(?P<source>\w+)$', views.OAuth2Authentication.as_view(), name="oauth"),
]

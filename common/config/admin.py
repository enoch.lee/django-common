from django.contrib import admin
from common.config.models import Configuration


class ConfigInline(admin.StackedInline):
    model = Configuration
    extra = 0


@admin.register(Configuration)
class ConfigAdmin(admin.ModelAdmin):
    list_display = ['name', 'category', 'description']
    inlines = [ConfigInline]


# coding=utf-8
from __future__ import unicode_literals
from django.core.validators import RegexValidator


class ContactValidator(RegexValidator):
    regex = r'^[0-9]*$'
    message = '숫자만 입력해주세요.'

# -*- coding: utf-8 -*-
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^pin/create/$', views.PinCreate.as_view(), name="pin-create"),
    url(r'^pin/verify/$', views.PinVerify.as_view(), name="pin-verify"),
]
